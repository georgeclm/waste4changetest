<?php
require 'model/TrashModel.php';
require_once 'config.php';

session_status() === PHP_SESSION_ACTIVE ? TRUE : session_start();

class TrashController
{

    function __construct()
    {
        $this->objconfig = new config();
        $this->objsm =  new TrashModel($this->objconfig);
    }
    // mvc handler request
    public function mvcHandler()
    {
        $act = isset($_GET['act']) ? $_GET['act'] : NULL;
        switch ($act) {
            case 'add':
                $this->insert();
                break;
            case 'delete':
                $this->delete();
                break;
            case 'material_list':
                $this->materialListApi();
                break;
            case 'trash_list':
                $this->trashListApi();
                break;
            default:
                $this->list();
        }
    }
    // page redirection
    public static function pageRedirect($url)
    {
        header('Location:' . $url);
    }
    // check validation
    public static function checkValidation($trash)
    {
        $noerror = true;
        if (empty($trash->material_id)) {
            $trash->material_id_msg = "Field is empty.";
            $noerror = false;
        } else {
            $trash->material_id_msg = "";
        }
        if (empty($trash->name)) {
            $trash->name_msg = "Field is empty.";
            $noerror = false;
        } else {
            $trash->name_msg = "";
        }
        return $noerror;
    }
    // add new record
    public function insert()
    {
        try {
            $trash = new TrashModel($this->objconfig);
            if (isset($_POST['addbtn'])) {
                // read form value
                $trash->material_id = trim($_POST['material_id']);
                $trash->name = trim($_POST['name']);
                //call validation
                $chk = $this->checkValidation($trash);
                if ($chk) {
                    //call insert record            
                    $pid = $this->objsm->insertRecord($trash);
                    if ($pid > 0) {
                        $this->list();
                    } else {
                        echo "Somthing is wrong..., try again.";
                    }
                } else {
                    $_SESSION['trashl0'] = serialize($trash); //add session obj   
                    // with error message        
                    $this->pageRedirect("view/insert.php");
                }
            }
        } catch (Exception $e) {
            echo "Somthing is wrong..., try again.";
            throw $e;
        }
    }
    // delete record
    public function delete()
    {
        try {
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $res = $this->objsm->deleteRecord($id);
                if ($res) {
                    $this->pageRedirect('index.php');
                } else {
                    echo "Somthing is wrong..., try again.";
                }
            } else {
                echo "Invalid operation.";
            }
        } catch (Exception $e) {
            echo "Somthing is wrong..., try again.";
            throw $e;
        }
    }
    public function list()
    {
        return $this->pageRedirect("view/list.php");
    }

    public function materialListApi()
    {
        header('Content-Type: application/json');
        $result = $this->objsm->material_list();
        $response = [];
        $materials = [];
        foreach ($result as $key => $res) {
            array_push($materials, ['id' => $key, 'name' => $res]);
        }
        array_push($response, ['materials' => $materials]);

        // echo json_encode($response[0]);
        // die();
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response[0]);
    }
    public function trashListApi()
    {
        header('Content-Type: application/json');
        $result = $this->objsm->selectRecord(0);
        $response = [];
        $types = [];
        while ($row = mysqli_fetch_array($result)) {
            array_push($types, ['id' => $row['id'], 'material_id' => $row['material_id'], 'name' => $row['name']]);
        }
        // Free result set
        mysqli_free_result($result);

        array_push($response, ['types' => $types]);

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response[0]);
    }
}
