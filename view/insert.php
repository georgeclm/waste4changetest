<?php
require '../model/TrashModel.php';
require_once '../config.php';
$con = new config();
session_start();
$trash = isset($_SESSION['trashl0']) ? unserialize($_SESSION['trashl0']) : new TrashModel($con);
$materials = $trash->material_list();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Add Trash</title>
    <style type="text/css">
        .bg-dark-blue {
            background-color: #0c325f !important;
        }

        .btn-light-blue {
            background-color: #30aee4 !important;
        }

        .wrapper {
            width: 500px;
            margin: 0 auto;
        }
    </style>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>
    <nav class="navbar navbar-light bg-dark-blue">
        <div class="container-fluid">
            <span class="navbar-brand mb-0 h1 text-white mx-auto">Input Sampah</span>
        </div>
    </nav>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form action="../index.php?act=add" method="post">
                        <div class="form-group mb-3 <?php echo (!empty($trash->material_id_msg)) ? 'has-error' : ''; ?> mt-3">
                            <label>Kategori Sampah</label>
                            <select class="form-select" required aria-label="Kategori" name="material_id">
                                <option value="" selected disabled hidden>Kategori</option>
                                <?php foreach ($materials as $key =>  $material) echo "<option value='$key'>$material</option>" ?>
                            </select>
                            <span class="help-block"><?php echo $trash->material_id_msg; ?></span>
                        </div>
                        <div class="form-group mb-3 <?php echo (!empty($trash->name_msg)) ? 'has-error' : ''; ?>">
                            <label>Nama Sampah</label>
                            <input name="name" class="form-control" placeholder="Nama Sampah" required value="<?php echo $trash->name; ?>">
                            <span class="help-block"><?php echo $trash->name_msg; ?></span>
                        </div>
                        <div class="d-grid gap-2">
                            <input type="submit" name="addbtn" class="btn btn-light-blue text-white rounded-3 py-2" value="Simpan">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>