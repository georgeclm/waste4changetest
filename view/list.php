<?php session_unset();
require '../model/TrashModel.php';
require_once '../config.php';
$con = new config();
$obj =  new TrashModel($con);
$result = $obj->selectRecord(0);
$materials = $obj->material_list();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Trash List</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style type="text/css">
        .wrapper {
            width: 650px;
            margin: 0 auto;
        }

        .btn-light-blue {
            background-color: #30aee4 !important;
        }

        .page-header h2 {
            margin-top: 0;
        }

        .bg-dark-blue {
            background-color: #0c325f !important;
        }

        a {
            color: red;
            text-decoration: none;
        }

        a:hover {
            color: red;
            text-decoration: none;
            cursor: pointer;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-light bg-dark-blue">
        <div class="container-fluid">
            <span class="navbar-brand mb-0 h1 text-white mx-auto">Data Sampah</span>
        </div>
    </nav>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-grid gap-2 my-3">
                        <a href="insert.php" class="btn btn-light-blue text-white py-2 rounded-3">Tambah</a>
                    </div>
                    <div class="d-grid gap-2 my-3">
                        <a href="../index.php?act=material_list" class="btn btn-light-blue text-white py-2 rounded-3">Material List API</a>
                    </div>
                    <div class="d-grid gap-2 my-3">
                        <a href="../index.php?act=trash_list" class="btn btn-light-blue text-white py-2 rounded-3">Trash List API</a>
                    </div>

                    <?php
                    if ($result->num_rows > 0) {
                        while ($row = mysqli_fetch_array($result)) {
                            echo "<div class='card shadow-lg rounded-3 mb-3'>
                            <div class='card-body'>
                                <div class='h5'>" . $row['name'] . "</div>
                                <div class='d-flex'>
                                    <div class='small text-muted flex-grow-1'>
                                        " . $materials[$row['material_id']] . "
                                    </div>
                                    <div class='small text-danger'>
                                    <a href='../index.php?act=delete&id=" . $row['id'] . "'>Hapus</a> 
                                </div>
                                </div>
                            </div>
                        </div>";
                        }
                        // Free result set
                        mysqli_free_result($result);
                    } else {
                        echo "<p class='lead'><em>No records were found.</em></p>";
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</body>

</html>