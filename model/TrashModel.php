<?php
require 'Trash.php';

interface Material
{
    public function material_list();
}
class TrashModel extends Trash implements Material
{
    // set database config for mysql  
    function __construct($consetup)
    {
        $this->host = $consetup->host;
        $this->user = $consetup->user;
        $this->pass =  $consetup->pass;
        $this->db = $consetup->db;
    }

    public function material_list()
    {
        try {
            $this->open_db();
            $query = $this->condb->prepare("SELECT * FROM materials");
            $query->execute();
            $res = $query->get_result();
            $query->close();
            $this->close_db();
            $result = [];
            if ($res->num_rows > 0) {
                while ($row = mysqli_fetch_array($res)) {
                    $result[$row['id']] = $row['name'];
                }
                // Free result set
                mysqli_free_result($res);
            } else {
                return $result;
            }
            return $result;
        } catch (Exception $e) {
            $this->close_db();
            throw $e;
        }
    }

    // open mysql data base  
    public function open_db()
    {
        $this->condb = new mysqli($this->host, $this->user, $this->pass, $this->db);
        if ($this->condb->connect_error) {
            die("Erron in connection: " . $this->condb->connect_error);
        }
    }
    // close database  
    public function close_db()
    {
        $this->condb->close();
    }
    // insert record  
    public function insertRecord($obj)
    {
        try {
            $this->open_db();
            $query = $this->condb->prepare("INSERT INTO trashs (material_id,name) VALUES (?, ?)");
            $query->bind_param("ss", $obj->material_id, $obj->name);
            $query->execute();
            $res = $query->get_result();
            $last_id = $this->condb->insert_id;
            $query->close();
            $this->close_db();
            return $last_id;
        } catch (Exception $e) {
            $this->close_db();
            throw $e;
        }
    }
    // delete record  
    public function deleteRecord($id)
    {
        try {
            $this->open_db();
            $query = $this->condb->prepare("DELETE FROM trashs WHERE id=?");
            $query->bind_param("i", $id);
            $query->execute();
            $res = $query->get_result();
            $query->close();
            $this->close_db();
            return true;
        } catch (Exception $e) {
            $this->close_db();
            throw $e;
        }
    }
    // select record       
    public function selectRecord($id)
    {
        try {
            $this->open_db();
            if ($id > 0) {
                $query = $this->condb->prepare("SELECT * FROM trashs WHERE id=?");
                $query->bind_param("i", $id);
            } else {
                $query = $this->condb->prepare("SELECT * FROM trashs");
            }

            $query->execute();
            $res = $query->get_result();
            $query->close();
            $this->close_db();
            return $res;
        } catch (Exception $e) {
            $this->close_db();
            throw $e;
        }
    }
}
